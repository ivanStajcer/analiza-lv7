using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        static String Igrac1, Igrac2;

        bool flag = true;

        int counter = 0;

        
        public Form1()
        {
            InitializeComponent();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Igrac2 = textBox2.Text;
        }


        public void Disable()
        {
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
            }
            catch { }

        }


        public void IsWin()
        {
            bool Winner = false;
            if ((btn1.Text == btn2.Text) && (btn2.Text == btn3.Text) && (!btn1.Enabled)) { Winner = true; }

            else if ((btn4.Text == btn5.Text) && (btn5.Text == btn6.Text) && (!btn4.Enabled)) { Winner = true; }

            else if ((btn7.Text == btn8.Text) && (btn8.Text == btn9.Text) && (!btn7.Enabled)) { Winner = true; }

            else if ((btn1.Text == btn4.Text) && (btn4.Text == btn7.Text) && (!btn1.Enabled)) { Winner = true; }

            else if ((btn2.Text == btn5.Text) && (btn5.Text == btn8.Text) && (!btn2.Enabled)) { Winner = true; }

            else if ((btn3.Text == btn6.Text) && (btn6.Text == btn9.Text) && (!btn3.Enabled)) { Winner = true; }

            else if ((btn1.Text == btn5.Text) && (btn5.Text == btn9.Text) && (!btn1.Enabled)) { Winner = true; }

            else if ((btn3.Text == btn5.Text) && (btn5.Text == btn7.Text) && (!btn3.Enabled)) { Winner = true; }

            int counter2 = 0;
            int counter3 = 0;
            int counter4 = 0;
            if (Winner)
            {
                Disable();
                String PlayWinner = "";
                if (flag)
                {
                    PlayWinner = Igrac1;
                    counter2++;
                }
                else
                {
                    PlayWinner = Igrac2;
                    counter3++;
                }
                MessageBox.Show("Pobjednik je:" + PlayWinner);
            }
            else if (counter == 9)
            {
                counter4++;
                MessageBox.Show("Nerijeseno je");
            }
            label5.Text = counter2.ToString() +" : "+ counter3.ToString();
            if (flag == true) label6.Text = Igrac1;

            if (flag != true) label6.Text = Igrac2;



        }

        private void btn1_Click(object sender, EventArgs e)
        {
            
            counter = 0;
            Button tempBtn = (Button)sender;
            if (flag)
            {
                tempBtn.Text = "X";
            }
            else
            {
                tempBtn.Text = "O";
            }
            flag = !flag;
            tempBtn.Enabled = false;
            counter++;
            IsWin();
        }

        private void btn2_Click(object sender, EventArgs e)
        {
          
            counter = 0;
            Button tempBtn = (Button)sender;
            if (flag)
            {
                tempBtn.Text = "X";
            }
            else
            {
                tempBtn.Text = "O";
            }
            flag = !flag;
            tempBtn.Enabled = false;
            counter++;
            IsWin();
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            counter = 0;
            Button tempBtn = (Button)sender;
            if (flag)
            {
                tempBtn.Text = "X";
            }
            else
            {
                tempBtn.Text = "O";
            }
            flag = !flag;
            tempBtn.Enabled = false;
            counter++;
            IsWin();
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            
            counter = 0;
            Button tempBtn = (Button)sender;
            if (flag)
            {
                tempBtn.Text = "X";
            }
            else
            {
                tempBtn.Text = "O";
            }
            flag = !flag;
            tempBtn.Enabled = false;
            counter++;
            IsWin();
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            
            counter = 0;
            Button tempBtn = (Button)sender;
            if (flag)
            {
                tempBtn.Text = "X";
            }
            else
            {
                tempBtn.Text = "O";
            }
            flag = !flag;
            tempBtn.Enabled = false;
            counter++;
            IsWin();
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            
            counter = 0;
            Button tempBtn = (Button)sender;
            if (flag)
            {
                tempBtn.Text = "X";
            }
            else
            {
                tempBtn.Text = "O";
            }
            flag = !flag;
            tempBtn.Enabled = false;
            counter++;
            IsWin();
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            
            counter = 0;
            Button tempBtn = (Button)sender;
            if (flag)
            {
                tempBtn.Text = "X";
            }
            else
            {
                tempBtn.Text = "O";
            }
            flag = !flag;
            tempBtn.Enabled = false;
            counter++;
            IsWin();
        }

        private void btn8_Click(object sender, EventArgs e)
        {
           
            counter = 0;
            Button tempBtn = (Button)sender;
            if (flag)
            {
                tempBtn.Text = "X";
            }
            else
            {
                tempBtn.Text = "O";
            }
            flag = !flag;
            tempBtn.Enabled = false;
            counter++;
            IsWin();
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            
            counter = 0;
            Button tempBtn = (Button)sender;
            if (flag)
            {
                tempBtn.Text = "X";
            }
            else
            {
                tempBtn.Text = "O";
            }
            flag = !flag;
            tempBtn.Enabled = false;
            counter++;
            IsWin();

        }

        private void button12_Click(object sender, EventArgs e)
        {
            label6.Text = Igrac1;


            btn1.Enabled = true;
            btn1.Text = "";
            btn2.Enabled = true;
            btn2.Text = "";
            btn3.Enabled = true;
            btn3.Text = "";
            btn4.Enabled = true;
            btn4.Text = "";
            btn5.Enabled = true;
            btn5.Text = "";
            btn6.Enabled = true;
            btn6.Text = "";
            btn7.Enabled = true;
            btn7.Text = "";
            btn8.Enabled = true;
            btn8.Text = "";
            btn9.Enabled = true;
            btn9.Text = "";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Igrac1 = textBox1.Text;
        }
    }
}
